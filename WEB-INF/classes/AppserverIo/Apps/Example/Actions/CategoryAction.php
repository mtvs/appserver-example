<?php namespace AppserverIo\Apps\Example\Actions;

use AppserverIo\Apps\Example\Entities\Impl\Category;
use AppserverIo\Psr\Servlet\Http\HttpServletRequestInterface;
use AppserverIo\Psr\Servlet\Http\HttpServletResponseInterface;
use AppserverIo\Routlt\DispatchAction;
use AppserverIo\Apps\Example\Utils\RequestKeys;

/**
 * Class CategoryAction
 * @package AppserverIo\Apps\Example\Actions
 *
 * @Path(name="/category")
 *
 * @Results({
 *     @Result(name="input", result="/dhtml/category.dhtml", type="AppserverIo\Routlt\Results\ServletDispatcherResult"),
 *     @Result(name="failure", result="/dhtml/category.dhtml", type="AppserverIo\Routlt\Results\ServletDispatcherResult")
 * })
 */
class CategoryAction extends DispatchAction
{
    /**
     * @var \AppserverIo\Apps\Example\Services\CategoryProcessor
     * @EnterpriseBean
     */
    private $categoryProcessor;

    /**
     * @return \AppserverIo\RemoteMethodInvocation\RemoteObjectInterface The instance
     */
    public function getCategoryProcessor()
    {
        return $this->categoryProcessor;
    }

    /**
     * @param HttpServletRequestInterface $servletRequest
     * @param HttpServletResponseInterface $servletResponse
     *
     * @Action(name="/index")
     */
    public function indexAction(HttpServletRequestInterface $servletRequest, HttpServletResponseInterface $servletResponse)
    {
        $servletRequest->setAttribute(RequestKeys::OVERVIEW_DATA, $this->getCategoryProcessor()->findAll());;
    }



    /**
     * @param HttpServletRequestInterface $servletRequest
     * @param HttpServletResponseInterface $servletResponse
     *
     * @Action(name="/persist")
     */
    public function persistAction(HttpServletRequestInterface $servletRequest, HttpServletResponseInterface $servletResponse)
    {
        $categoryId = $servletRequest->getParameter(RequestKeys::CATEGORY_ID, FILTER_VALIDATE_INT);

        if ($name = trim($servletRequest->getParameter(RequestKeys::NAME)))
        {
            $entity = new Category();
            $entity->setId($categoryId);
            $entity->setName($name);

            $this->getCategoryProcessor()->persist($entity);

            $servletRequest->setAttribute(RequestKeys::OVERVIEW_DATA, $this->getCategoryProcessor()->findAll());
        }
        else
        {
            $this->addFieldError(RequestKeys::NAME, 'Please add a name!');
        }
    }

    /**
     * @param HttpServletRequestInterface $servletRequest
     * @param HttpServletResponseInterface $servletResponse
     * @throws \Exception
     *
     * @Action(name="/load")
     */
    public function loadAction(HttpServletRequestInterface $servletRequest, HttpServletResponseInterface $servletResponse)
    {
        // check if the necessary params has been specified and are valid
        $categoryId = $servletRequest->getParameter(RequestKeys::CATEGORY_ID, FILTER_VALIDATE_INT);
        if ($categoryId == null) {
            throw new \Exception(sprintf('Can\'t find requested %s', RequestKeys::CATEGORY_ID));
        }

        // load the entity to be edited and attach it to the servlet context
        $viewData = $this->getCategoryProcessor()->load($categoryId);
        $servletRequest->setAttribute(RequestKeys::VIEW_DATA, $viewData);

        // append the sample data to the request attributes
        $servletRequest->setAttribute(RequestKeys::OVERVIEW_DATA, $this->getCategoryProcessor()->findAll());
    }

    /**
     * @param HttpServletRequestInterface $servletRequest
     * @param HttpServletResponseInterface $servletResponse
     * @throws \Exception
     *
     * @Action(name="/delete")
     */
    public function deleteAction(HttpServletRequestInterface $servletRequest, HttpServletResponseInterface $servletResponse)
    {

        // check if the necessary params has been specified and are valid
        $categoryId = $servletRequest->getParameter(RequestKeys::CATEGORY_ID, FILTER_VALIDATE_INT);
        if ($categoryId == null) {
            throw new \Exception(sprintf('Can\'t find requested %s', RequestKeys::CATEGORY_ID));
        }

        // delete the entity
        $this->getCategoryProcessor()->delete($categoryId);

        // append the sample data to the request attributes
        $servletRequest->setAttribute(RequestKeys::OVERVIEW_DATA, $this->getCategoryProcessor()->findAll());
    }

}