<?php namespace AppserverIo\Apps\Example\Services;

use AppserverIo\Apps\Example\Entities\Impl\Category;

/**
 * Class CategoryProcessor
 * @package AppserverIo\Apps\Example\Services
 *
 * @Stateless
 */
class CategoryProcessor extends AbstractProcessor implements CategoryProcessorInterface
{

    /**
     * Returns an array with all existing entities.
     *
     * @param integer $limit The maxium number of rows to return
     * @param integer $offset The row to start with
     *
     * @return array An array with all existing entities
     */
    public function findAll($limit = 100, $offset = 0)
    {
        $entityManager = $this->getEntityManager();
        $repository = $entityManager->getRepository(Category::class);
        return $repository->findBy(array(), array(), $limit, $offset);
    }

    /**
     * Persists the passed entity.
     *
     * @param \AppserverIo\Apps\Example\Entities\Impl\Category $entity The entity to persist
     *
     * @return \AppserverIo\Apps\Example\Entities\Impl\Category The persisted entity
     */
    public function persist(Category $entity)
    {
        // load the entity manager
        $entityManager = $this->getEntityManager();
        // check if a detached entity has been passed
        if ($entity->getId()) {
            $merged = $entityManager->merge($entity);
            $entityManager->persist($merged);
        } else {
            $entityManager->persist($entity);
        }
        // flush the entity manager
        $entityManager->flush();
        // and return the entity itself
        return $entity;
    }

    public function load($id)
    {
        return $this->getEntityManager()->find(Category::class, $id);
    }

    public function delete($id)
    {
        $entityManager = $this->getEntityManager();
        $this->getEntityManager()->remove($entityManager->merge($this->load($id)));
        $entityManager->flush();
    }
}