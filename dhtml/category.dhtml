<?php

namespace AppserverIo\Apps\Example\Servlets;

use AppserverIo\Apps\Example\Utils\ViewHelper;
use AppserverIo\Apps\Example\Utils\RequestKeys;

// initialize local variables
$categoryId = 0;
$name = '';

// load the entity data if available
if ($entity = $servletRequest->getAttribute(RequestKeys::VIEW_DATA)) {
    $categoryId = $entity->getId();
    $name = $entity->getName();
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="appserver.io">

    <title>appserver.io example</title>

    <base href="<?php echo ViewHelper::singleton()->getBaseUrl($servletRequest) ?>">

    <link href="components/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="static/css/style.css" rel="stylesheet">
    <link href="static/img/favicon.png" rel="shortcut icon">
</head>

<body>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.do"><img src="static/img/appserver-io-example.png" alt="appserver.io example"></a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li>
                    <p class="navbar-text"><?php if (ViewHelper::singleton()->isLoggedIn($servletRequest)): ?>Logged in as: <strong><?php echo ViewHelper::singleton()->getUsername($servletRequest) ?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="<?php echo ViewHelper::singleton()->getLogoutLink() ?>">logout</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="<?php echo ViewHelper::singleton()->getUserEditLink() ?>">edit</a><?php else: ?>Not logged in.<?php endif; ?></p>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="index.do"><span class="glyphicon glyphicon-home"></span>&nbsp;&nbsp;&nbsp;Home</a></li>
                <li  class="active"><a href="index.do/category"><span class="glyphicon glyphicon-star-empty"></span>&nbsp;&nbsp;&nbsp;Categories</a></li>
                <li><a href="dhtml/login.dhtml"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;&nbsp;Login</a></li>
                <li><a href="dhtml/upload.dhtml"><span class="glyphicon glyphicon-upload"></span>&nbsp;&nbsp;&nbsp;Upload</a></li>
                <li><a href="index.do/import"><span class="glyphicon glyphicon-list"></span>&nbsp;&nbsp;&nbsp;Import</a></li>
                <li><a href="index.do/product"><span class="glyphicon glyphicon-home"></span>&nbsp;&nbsp;&nbsp;Products</a></li>
                <li><a href="index.do/cart"><span class="glyphicon glyphicon-home"></span>&nbsp;&nbsp;&nbsp;Cart</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-briefcase"></span>&nbsp;&nbsp;&nbsp;Authentication <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="dhtml/basic.dhtml/authentication">Basic</a></li>
                        <li><a href="dhtml/digest.dhtml/authentication">Digest</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="container content">

    <?php if ($errorMessages = $servletRequest->getAttribute(RequestKeys::ERROR_MESSAGES)): ?>
        <div class="alert alert-danger">
            <?php foreach ($errorMessages as $errorMessage): ?>
                <?php echo $errorMessage ?><br/>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>

    <a href="http://appserver.io">
        <img src="static/img/logo-black.png"/>
    </a>

    <br>

    <h1>Categories</h1>

    <br>

    <form action="index.do/category/persist" method="post" class="form-inline" role="form">
        <div class="form-group">
            <input name="<?php echo RequestKeys::CATEGORY_ID ?>" type="hidden" value="<?php echo $categoryId ?>"/>
            <input name="<?php echo RequestKeys::NAME ?>" type="text" required="" placeholder="Type the category name ..." class="form-control" value="<?php echo $name ?>"/>
            <button type="submit" class="btn btn-defalt">Submit</button>
        </div>
    </form>

    <br><br>

    <table class="table table-hover">
        <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($servletRequest->getAttribute(RequestKeys::OVERVIEW_DATA) as $entity): ?>
            <tr>
                <td><a href="index.do/category/load?categoryId=<?php echo $entity->getId() ?>"><?php echo $entity->getId() ?></a>
                </td>
                <td><?php echo $entity->getName() ?></td>
                <td><a href="index.do/category/load?categoryId=<?php echo $entity->getId() ?>">Edit</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="index.do/category/delete?categoryId=<?php echo $entity->getId() ?>">Delete</a></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <div class="footer">
        <p>&copy; and built with pride by <a href="http://appserver.io" target="_blank">appserver.io</a></p>
    </div>

</div>

<!-- placed at the end of the document so the pages load faster -->
<script src="components/jquery/jquery.min.js"></script>
<script src="components/bootstrap/js/bootstrap.min.js"></script>

</body>
</html>