<?php
/**
 * Created by PhpStorm.
 * User: Q702
 * Date: 4/9/2016
 * Time: 4:52 PM
 */

namespace AppserverIo\Apps\Example\Services;


interface CategoryProcessorInterface
{
    /**
     * Returns an array with all existing entities.
     *
     * @param integer $limit  The maxium number of rows to return
     * @param integer $offset The row to start with
     *
     * @return array An array with all existing entities
     */
    public function findAll($limit = 100, $offset = 0);
}